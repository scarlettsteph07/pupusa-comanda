import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import PropTypes from 'prop-types';

import { Ticket, BoardWrapper } from './components';
import { LinkButton, LinkButtonWrapper, OrderType } from '../../common';
import { orderActions } from '../../actions';

class Board extends Component {
  static propTypes = {
    orders: PropTypes.objectOf(OrderType).isRequired,
    dispatchGetOrders: PropTypes.func.isRequired,
  }
  componentDidMount = () => {
    this.props.dispatchGetOrders();
  }

  render() {
    return (
      <div>
        <LinkButtonWrapper>
          <LinkButton to="/order">New Order</LinkButton>
        </LinkButtonWrapper>
        <BoardWrapper>
          {
          _.map(this.props.orders, order => (
            <Ticket
              key={order._id}
              order={order}
            />
          ))
        }
        </BoardWrapper>
      </div>
    );
  }
}

const mapStateToProps = reduxState => ({
  orders: reduxState.orders,
});

const mapDispatchToProps = {
  dispatchAddOrder: orderActions.addOrder,
  dispatchGetOrders: orderActions.getOrders,
};

export default connect(mapStateToProps, mapDispatchToProps)(Board);

