import React from 'react';

import { OrderWrapper, OrderHeader, OrderBody, Quantity } from './StyledComponents';
import { OrderType } from '../../../common';

export const Ticket = ({ order }) => (
  <OrderWrapper>
    <OrderHeader>Order #{order.orderNumber}</OrderHeader>
    <OrderBody>
      <div>Arroz</div>
      <ul>
        <li>Queso: <Quantity>{order.arroz.queso}</Quantity></li>
        <li>Frijol Con Queso: <Quantity>{order.arroz.frijolQueso}</Quantity></li>
        <li>Revueltas: <Quantity>{order.arroz.revuelta}</Quantity></li>
      </ul>
      <div>Maiz</div>
      <ul>
        <li>Queso: <Quantity>{order.maiz.queso}</Quantity></li>
        <li>Frijol Con Queso: <Quantity>{order.maiz.frijolQueso}</Quantity></li>
        <li>Revueltas: <Quantity>{order.maiz.revuelta}</Quantity></li>
      </ul>
    </OrderBody>
  </OrderWrapper>
);

Ticket.propTypes = {
  order: OrderType.isRequired,
};

