import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import thunk from 'redux-thunk';

import OrderForm from './OrderForm';
import Board from './Board';
import { theme, SlatInner, SlatOuter } from '../common';
import { Error404 } from '../common/components';
import { rootReducer } from '../reducers';

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

const App = () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <SlatOuter>
        <SlatInner>
          <Router>
            <Switch>
              <Route
                exact
                path="/"
                component={Board}
              />
              <Route
                exact
                path="/order"
                component={OrderForm}
              />
              <Route component={Error404} />
            </Switch>
          </Router>
        </SlatInner>
      </SlatOuter>
    </ThemeProvider>
  </Provider>
);

export default App;
