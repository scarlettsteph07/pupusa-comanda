import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {
  Counter, FormWrapper, Row,
  Cell, HeaderRow, FlavourCell,
} from './components';
import { LinkButton, LinkButtonWrapper } from '../../common';
import { orderActions } from '../../actions';

class OrderForm extends Component {
  static defaultProps = {
    errorMessage: '',
  }

  static propTypes = {
    dispatchPostOrder: PropTypes.func.isRequired,
    errorMessage: PropTypes.string,
    history: PropTypes.shape({
      push: PropTypes.func,
    }).isRequired,
  }

  state = {
    order: {
      arroz: {
        queso: 0,
        frijolQueso: 0,
        revuelta: 0,
      },
      maiz: {
        queso: 0,
        frijolQueso: 0,
        revuelta: 0,
      },
    },
  }

  add = (masa, tipo) => {
    this.setState(prevState => ({
      order: _.set(
        prevState.order,
        `${masa}.${tipo}`,
        // prevState.order[masa][tipo] + 1,
        `${prevState.order[masa][tipo]}hola`,
      ),
    }));
  }

  subtract = (masa, tipo) => {
    this.setState(prevState => ({
      order: _.set(
        prevState.order,
        `${masa}.${tipo}`,
        Math.max(0, prevState.order[masa][tipo] - 1),
      ),
    }));
  }

  addOrder = (event) => {
    event.preventDefault();
    const orderToPost = this.state.order;
    this.props.dispatchPostOrder(orderToPost, this.props.history.push);
  }

  render() {
    return (
      <FormWrapper>
        <LinkButtonWrapper alignment="left">
          <LinkButton to="/">Board</LinkButton>
        </LinkButtonWrapper>
        <h1>{this.props.errorMessage}</h1>
        <HeaderRow>
          <Cell />
          <Cell className="example">Arroz</Cell>
          <Cell>Maiz</Cell>
        </HeaderRow>
        <Row>
          <FlavourCell> Queso </FlavourCell>
          <Cell>
            <Counter
              count={this.state.order.arroz.queso}
              add={() => this.add('arroz', 'queso')}
              subtract={() => this.subtract('arroz', 'queso')}
            />
          </Cell>
          <Cell>
            <Counter
              count={this.state.order.maiz.queso}
              add={() => this.add('maiz', 'queso')}
              subtract={() => this.subtract('maiz', 'queso')}
            />
          </Cell>
        </Row>
        <Row>
          <FlavourCell>Frijol Con Queso</FlavourCell>
          <Cell>
            <Counter
              count={this.state.order.arroz.frijolQueso}
              add={() => this.add('arroz', 'frijolQueso')}
              subtract={() => this.subtract('arroz', 'frijolQueso')}
            />
          </Cell>
          <Cell>
            <Counter
              count={this.state.order.maiz.frijolQueso}
              add={() => this.add('maiz', 'frijolQueso')}
              subtract={() => this.subtract('maiz', 'frijolQueso')}
            />
          </Cell>
        </Row>
        <Row>
          <FlavourCell>Revuelta</FlavourCell>
          <Cell>
            <Counter
              count={this.state.order.arroz.revuelta}
              add={() => this.add('arroz', 'revuelta')}
              subtract={() => this.subtract('arroz', 'revuelta')}
            />
          </Cell>
          <Cell>
            <Counter
              count={this.state.order.maiz.revuelta}
              add={() => this.add('maiz', 'revuelta')}
              subtract={() => this.subtract('maiz', 'revuelta')}
            />
          </Cell>
        </Row>
        <LinkButtonWrapper>
          <LinkButton to="/" onClick={this.addOrder}>Add Order</LinkButton>
        </LinkButtonWrapper>
      </FormWrapper>
    );
  }
}

// const mapDispatchToProps = dispatch => ({
//   dispatchAddOrder: () => dispatch(orderActions.addOrder()),
// });

// const mapDispatchToProps = (dispatch) => {
//   const boundActionCreators = bindAtionCreators({
//     order: orderActions.addOrder,
//   });
//   return boundActionCreators;
// };

const mapStateProps = reduxState => ({
  errorMessage: reduxState.app.errorMessage,
});

const mapDispatchToProps = {
  dispatchPostOrder: orderActions.postOrder,
};

export default connect(mapStateProps, mapDispatchToProps)(OrderForm);
