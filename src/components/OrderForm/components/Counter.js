import React from 'react';
import PropTypes from 'prop-types';

import { CounterButton, CounterWrapper } from './StyledComponents';

export const Counter = ({ subtract, count, add }) => (
  <CounterWrapper>
    <CounterButton onClick={subtract}>-</CounterButton>
    <span>{count}</span>
    <CounterButton onClick={add}>+</CounterButton>
  </CounterWrapper>
);

Counter.propTypes = {
  count: PropTypes.number.isRequired,
  add: PropTypes.func.isRequired,
  subtract: PropTypes.func.isRequired,
};
