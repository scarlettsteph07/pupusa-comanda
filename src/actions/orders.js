import axios from 'axios';
import _ from 'lodash';

import { orderTypes } from '../actionTypes';

export const addOrder = payload => ({
  type: orderTypes.ADD_ORDER,
  payload,
});

export const successGetOrders = payload => ({
  type: orderTypes.SUCCESS_GET_ORDERS,
  payload,
});

export const failedGetOrders = payload => ({
  type: orderTypes.FAILED_GET_ORDERS,
  payload,
});

export const failedPostOrders = payload => ({
  type: orderTypes.FAILED_POST_ORDERS,
  payload,
});

export const ordersNotResults = () => ({
  type: orderTypes.NO_ORDER_RESULTS,
});

export const getOrders = () => (dispatch, getState) => {
  const currentOrders = getState().orders;
  if (_.size(currentOrders) === 0) {
    axios.get('https://pupusa-comanda.herokuapp.com/api/public/orders')
      .then((response) => {
        const orders = _.keyBy(response.data, '_id');
        dispatch(successGetOrders(orders));
      })
      .catch((error) => {
        dispatch(failedGetOrders(error));
      });
  } else {
    dispatch(ordersNotResults());
  }
};

export const postOrder = (orderParams, push) => (dispatch) => {
  axios.post('https://pupusa-comanda.herokuapp.com/api/public/orders', orderParams)
    .then((response) => {
      const newOrder = response.data;
      dispatch(addOrder(newOrder));
      push('/');
    })
    .catch((error) => {
      dispatch(failedPostOrders(error));
    });
};

