import { orderTypes } from '../actionTypes';

export const orders = (state = {}, action) => {
  switch (action.type) {
    case orderTypes.ADD_ORDER:
      return {
        ...state,
        [action.payload._id]: action.payload,
      };
    case orderTypes.SUCCESS_GET_ORDERS:
      return action.payload;
    case orderTypes.FAILED_GET_ORDERS:
      return state;
    case orderTypes.NO_ORDER_RESULTS:
      return state;
    default:
      return state;
  }
};

