import { orderTypes } from '../actionTypes';

export const app = (state = {}, action) => {
  switch (action.type) {
    case orderTypes.FAILED_POST_ORDERS:
      return {
        ...state,
        errorMessage: action
          .payload
          .response
          .data
          .message,
      };
    default:
      return state;
  }
};

