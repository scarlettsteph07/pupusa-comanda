import { combineReducers } from 'redux';

import { orders } from './orders';
import { app } from './app';

export const rootReducer = combineReducers({
  orders,
  app,
});
