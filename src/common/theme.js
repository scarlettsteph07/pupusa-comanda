export const theme = {
  black: '#000',
  white: '#fff',
  gray: 'rgba(225, 224, 225, 0.72)',
  lightGray: 'rgba(225, 224, 225, 0.2)',
  lightGray2: '#B5B5B5',
  lightBlue: '#DFE8EC',
  brownPaperBag: '#B29772',
  brown: '#35281E',
  transparent: 'transparent',
  baseFont: '"Trebuchet MS", sans-serif',
  large: {
    before: '1139px',
    inner: '1140px',
    start: '1141px',
    end: '1440px',
  },
  xlarge: {
    outer: '1440px',
    inner: '1140px',
    start: '1441px',
  },
};
