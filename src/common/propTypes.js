import PropTypes from 'prop-types';

export const OrderDetailType = PropTypes.shape({
  queso: PropTypes.number.isRequired,
  frijolQueso: PropTypes.number.isRequired,
  revuelta: PropTypes.number.isRequired,
});

export const OrderType = PropTypes.shape({
  _id: PropTypes.string.isRequired,
  orderNumber: PropTypes.number.isRequired,
  arroz: OrderDetailType.isRequired,
  maiz: OrderDetailType.isRequired,
});
