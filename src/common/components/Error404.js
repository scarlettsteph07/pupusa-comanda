import React from 'react';
import styled from 'styled-components';

export const Error404 = () => (
  <Wrapper>
    <h1>
      404 - Pupusas not found
    </h1>
  </Wrapper>
);

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;

